package websocketserver.beans;

import java.util.List;

public class Message {
	private String className;
	private String methodName;
	private List<Object> parameters;
	
	public Message(){
	}
	
	public Message(String className, String methodName, List<Object> parameters){
		this.className = className;
		this.methodName = methodName;
		this.parameters = parameters;
	}

	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
	
	public String getMethodName() {
		return methodName;
	}
	
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public List<Object> getParameters() {
		return parameters;
	}
	
	public void setParameters(List<Object> parameters) {
		this.parameters = parameters;
	}
}
