package websocketserver.actions;

import javax.websocket.Session;

import websocketserver.beans.Message;
import websocketserver.server.WebSocketServer;

public abstract class Action {
	
	private Session session;
	private WebSocketServer webSocketServer;
	
	public void init(Action action) {
		this.session = action.getSession();
		this.webSocketServer = action.getWebSocketServer();
	}

	public void setSession(Session session){
		this.session = session;
	}
	
	public Session getSession(){
		return session;
	}
	
	public void setWebSocketServer(WebSocketServer webSocketServer) {
		this.webSocketServer = webSocketServer;
	}
	
	public WebSocketServer getWebSocketServer() {
		return webSocketServer;
	}
	
	public void sendMessage(Session session, Message message) {
		try {
			session.getBasicRemote().sendText(getWebSocketServer().convertData(message));
		} catch (Exception ioe) {
			try {
				Thread.sleep(100);
				sendMessage(session, message);
	        } catch (InterruptedException e) {
	            System.out.println(e.getMessage());
	        }
		}
	}	
}
