package websocketserver.actions;

import java.util.ArrayList;
import java.util.List;
import websocketserver.beans.Message;

public class SystemAction extends Action {
	
	public Message ping(List<Object> parameters) throws Exception {
		List<Object> outParameters = new ArrayList<Object>();
		Message responseMessage = new Message("SystemAction","pong",outParameters);
		return responseMessage;
	}	
}
