package websocketserver.server;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import beans.User;
import websocketserver.beans.Message;

@ServerEndpoint("/actions")
public class WebSocketServer {

	private static HashMap<String, Session> sessions;
	private static Boolean initialize = true;
	private static ObjectMapper objectMapper;
	
	public WebSocketServer(){
		if (initialize) {
			sessions = new HashMap<String, Session>();
			objectMapper = new ObjectMapper(); 
			initialize = false;
		}
	}
	
	public synchronized HashMap<String, Session> getSessions(){
		return sessions;
	}
	
	public synchronized Message convertData(String data) throws Exception {
		Message message = objectMapper.readValue(data, Message.class);    			
		return message;
	}
	
	public synchronized String convertData(Message data) throws Exception {
		String message = objectMapper.writeValueAsString(data); 			
		return message;
	}	

    @OnOpen
    public synchronized void open(Session session, EndpointConfig conf) {
        try {
	        sessions.put(session.getId(),session);
    	} catch (Exception e){
    		processError(session, e, "Open connection");
    	}	        
    }
    
    @OnMessage
    public synchronized void message(Session session, String data) {
    	try { 		
    		process(session, data);
    	} catch (Exception e){
    		processError(session, e, data);
    	}
    }  
    
    @OnClose
    public synchronized void close(Session session, CloseReason closeReason) {
        try {
	        sessions.remove(session.getId());   
    	} catch (Exception e){
    		processError(session, e, "Close session");
    	}
    }
    
    @OnError
    public synchronized void error(Session session, Throwable error) {
        System.out.println(String.format("Session error because of %s. %s", session.getId(), error.getMessage()));
    }  
    
    private synchronized void process(Session session, String data){
    	try {
    		Message message = convertData(data);
    		Class clazz = Class.forName(message.getClassName());
    		Object object = clazz.newInstance();
    		Method method = clazz.getMethod("setSession", Session.class);
    		method.invoke(object, session);
    		method = clazz.getMethod("setWebSocketServer",WebSocketServer.class);
    		method.invoke(object, this);
    		Message returnMessage = null;
    		if (message.getParameters() != null) {
    			method = clazz.getDeclaredMethod(message.getMethodName(), List.class);
        		returnMessage = (Message)method.invoke(object, message.getParameters());
    		} else {
    			method = clazz.getMethod(message.getMethodName());
        		returnMessage = (Message)method.invoke(object);
    		}
    		if (returnMessage != null) {
    			session.getBasicRemote().sendText(convertData(returnMessage));
    		}
    	} catch (Exception e) {
    		processError(session, e, data);
    	}
    } 
    
    public synchronized void processError(Session session, Exception e, String data){
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("Process Error: Exception "+e.getMessage()+" Message:"+data);
		Message message = new Message("WebSocketServer","displayMessageError",parameters);
		try {
			session.getBasicRemote().sendText(convertData(message));
		} catch (Exception e1){
			error(session, e1);
		}
    }
}
