package actions;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.Session;

import beans.User;
import websocketserver.actions.Action;
import websocketserver.beans.Message;

public class MainAction extends Action {
	
	public MainAction() {
	}
	
	public void publishUpdates() throws Exception {
		System.out.println("publishUpdates");
		List<Object> outParameters = new ArrayList<Object>();
		List<User> localUsers = new ArrayList<User>();
		User currentUser = (User)getSession().getUserProperties().get("User");
		String currentUserPosition[] = currentUser.getPosition().split(",");
		for (Session session:getWebSocketServer().getSessions().values()) {
			User user = (User)session.getUserProperties().get("User");
			String userPosition[] = user.getPosition().split(",");
			if (Double.valueOf(userPosition[0]) >= Double.valueOf(currentUserPosition[0])-.10 &&
				Double.valueOf(userPosition[0]) <= Double.valueOf(currentUserPosition[0])+.10 &&
				Double.valueOf(userPosition[1]) >= Double.valueOf(currentUserPosition[1])-.10 &&
				Double.valueOf(userPosition[1]) <= Double.valueOf(currentUserPosition[1])+.10 &&
				user.getHelpNeeded() != null) {
				localUsers.add(user);
			}
		}
		outParameters.add(localUsers);
		Message responseMessage = new Message("MainAction","refresh",outParameters);	
		for (Session session:getWebSocketServer().getSessions().values()) {
			if (session != null &&
				session.isOpen()) {
				if (session.getUserProperties().get("User") != null) {	
					sendMessage(session, responseMessage);
				} 
			} else {
				getWebSocketServer().getSessions().remove(session.getId());
			}
		}
	}
	
	public Message addHelpNeeded(List<Object> parameters) throws Exception {
		List<Object> outParameters = new ArrayList<Object>();	
		Message responseMessage = null;
		User user = (User)getSession().getUserProperties().get("User");
		if (user != null) {
			System.out.println("UserID: "+user.getUserId()+" - addHelpNeeded: "+parameters);			
			user.setHelpNeeded((String)parameters.get(0));
			publishUpdates();
			responseMessage = new Message("MainAction","addHelpNeededResponse",outParameters);
		}
		return responseMessage;
	}
	
	public Message removeHelpNeeded(List<Object> parameters) throws Exception {
		List<Object> outParameters = new ArrayList<Object>();	
		Message responseMessage = null;
		User user = (User)getSession().getUserProperties().get("User");
		if (user != null) {
			System.out.println("UserID: "+user.getUserId()+" - removeHelpNeeded: "+parameters);
			user.setHelpNeeded(null);
			publishUpdates();
			responseMessage = new Message("MainAction","removeHelpNeededResponse",outParameters);
		}
		return responseMessage;
	}
	
	public Message updatePosition(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		User user = (User)getSession().getUserProperties().get("User");
		if (user != null) {
			System.out.println("UserID: "+user.getUserId()+" - updatePosition: "+parameters);
			user.setPosition((String)parameters.get(0));
			publishUpdates();
		}
		return responseMessage;
	}
}
