package actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.websocket.Session;

import beans.User;
import websocketserver.actions.Action;
import websocketserver.beans.Message;

public class LoginAction extends Action {
	
	public LoginAction() {
	}

	public Message login(List<Object> parameters) throws Exception {
		List<Object> outParameters = new ArrayList<Object>();	
		Message responseMessage = null;
		HashMap data = (HashMap)parameters.get(0);
		User user = new User((String)data.get("userId"),(String)data.get("phone"),null);
		if (user != null && user.getUserId() != null) {
			System.out.println("login: "+user.getUserId());
			if (getWebSocketServer().getSessions().get(user.getUserId()) != null) {
				outParameters.add("User ID already exists, try another");
			} else {
				outParameters.add("Success");
				getSession().getUserProperties().put("User", user);
			}
			responseMessage = new Message("LoginAction","loginResponse",outParameters);
		} else {	
			try { 
				getSession().close(); 
			} catch (IOException ioe) { 
				outParameters.add(ioe.getMessage()); 
			} 
		}
		return responseMessage;
	}
	
	public Message logout(List<Object> parameters) throws Exception {
		List<Object> outParameters = new ArrayList<Object>();	
		Message responseMessage = null;
		User user = (User)getSession().getUserProperties().get("User");
		if (user != null) {
			System.out.println("logout: "+user.getUserId());
			user.setHelpNeeded(null);
			MainAction main = new MainAction();
			main.init(this);
			main.publishUpdates();
			try { 
				Session session = getSession(); 
				session.close(); 
				getWebSocketServer().getSessions().remove(session.getId());
			} catch (IOException ioe) { 
				outParameters.add(ioe.getMessage()); 
			} 
		}
		return responseMessage;
	}
}
