// Constants
const canvas = document.getElementById("canvas");
const context = canvas.getContext("2d");

// Variables
var buttons = new Map();
var tiles = new Map();

// Classes
class Tile {
	
	constructor(id, imgSrc, backgroundColor, borderColor, selectedBorderColor, lineWidth, x, y, width, height, action) {
		this.id = id;
		this.imgSrc = imgSrc;
		this.backgroundColor = backgroundColor;
		this.borderColor = borderColor;
		this.selectedBorderColor = selectedBorderColor;
		this.lineWidth = lineWidth;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.action = action;
		this.selected = false;
	}
	
	draw() {
		if (this.imgSrc) {
			drawImage(this.imgSrc, this.x, this.y, this.width, this.height);
		}
		drawRect(this.x, this.y, this.width, this.height, this.backgroundColor, (this.borderColor?true:false), this.lineWidth, (this.selected?this.selectedBorderColor:this.borderColor), true);			
	}
	
	selected() {
		return this.selected;
	}
	
	clicked(mouseX, mouseY){
		let clicked = false;
		if (mouseX >= this.x-5 &&
			mouseX <= this.x+this.width+5 &&
			mouseY >= this.y+25 &&
			mouseY <= this.y+this.height+35){	
			if (this.action) {
				clicked = true;
			}
		}
		return clicked;
	}
	
	activate() {
		this.action();
	}
}

class Button {
	
	constructor(id, text, textColor, backgroundColor, x, y, width, height, action) {
		this.id = id;
		this.text = text;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.action = action;
	}
	
	draw() {
		let centerX = this.x+(this.width/2);
		let centerY = this.y+(this.height/2);
		context.font = "12pt Times New Roman";
		let textWidth = context.measureText(this.text).width;	
		context.beginPath();
		context.rect(this.x, this.y, this.width, this.height);
		context.fillStyle = this.backgroundColor;
		context.fill();
	    context.strokeStyle = "#000000";
	    context.stroke();	
	    context.closePath();
	    context.fillStyle = this.textColor;
		context.fillText(this.text, centerX-(textWidth/2), centerY+5);
	}
	
	clicked(mouseX, mouseY){
		let clicked = false;
		if (mouseX >= this.x &&
			mouseX <= this.x+this.width &&
			mouseY >= this.y &&
			mouseY <= this.y+this.height){		
			if (this.action) {
				clicked = true;
			}
		}
		return clicked;
	}
	
	activate() {
		this.action();
	}
}

// Events
window.addEventListener("resize", function(event) {
    resize();
})

// Functions
function resize() {
	console.log("Resize");
	canvas.width = window.innerWidth-15;
	canvas.height = window.innerHeight-50;
	if (typeof resizeScreen === "function") {
		resizeScreen();
	}
}

function init(){
	canvas.width = window.innerWidth-25;
	canvas.height = window.innerHeight-60;
	canvas.onmousedown = function(e) { handleMouseClick(e); }
	document.body.onkeydown = function(e) { handleKeyDown(e); };		
}

function handleMouseClick(e){
    let clickedButton = null;
	// handle buttons
	for (let [key, value] of buttons.entries()){
		if (value.clicked(e.x, e.y)){
			clickedButton = value;
			break;
		}
	}
	if (clickedButton){
		clickedButton.activate();
	}	
	
	// handle tiles
	let clickedTile = null;
	for (let [key, value] of tiles.entries()) {
		if (value.clicked(e.x, e.y)){
			clickedTile = value;
		}
	}
	if (clickedTile){
		clickedTile.activate();
	}	
}

function clearButtons(){
	buttons = new Map();
}

function clearTiles() {
	tiles = new Map();
}

function drawButton(id, text, borderColor, backgroundColor, x, y, width, height, action){
	context.globalCompositeOperation = "source-over";   		
	var button = new Button(id, text, borderColor, backgroundColor, x, y, width, height, action);
	button.draw();
	if (action) {
		buttons.set(id, button);
	}
}

function drawTile(id, imgSrc, backgroundColor, borderColor, selectedBorderColor, lineWidth, x, y, width, height, action){
	context.globalCompositeOperation = "source-over";   
	var tile = new Tile(id, imgSrc, backgroundColor, borderColor, selectedBorderColor, lineWidth, x, y, width, height, action);
	tile.draw();
	if (action) {
		tiles.set(id, tile);
	}
}

function drawText(x, y, font, fillStyle, text) {
	context.globalCompositeOperation = "source-over"; 
	context.font = font;
	context.fillStyle = fillStyle;
	context.fillText(text, x, y);	
}

function drawImage(imgSrc, x, y, width, height) {
	context.globalCompositeOperation = "source-over"; 
	let image = new Image();
	image.src = imgSrc;
	image.onload = function() {
		context.drawImage(image, x, y, width, height);		
	}
}

function drawLine(fromX, fromY, toX, toY, lineWidth, lineColor, srcOver) {
	context.beginPath();
    if (srcOver) {
    	context.globalCompositeOperation = "source-over";  	
    } else {
    	context.globalCompositeOperation = "destination-over";   	
    }	
	context.moveTo(fromX, fromY);
	context.lineTo(toX, toY);
	context.lineWidth = lineWidth;
	context.strokeStyle = lineColor;
	context.stroke();	
	context.closePath();
}

function drawCircle(x, y, radius, backgroundColor, border, borderSize, borderColor, srcOver) {
	context.beginPath();
    if (srcOver) {
    	context.globalCompositeOperation = "source-over";  	
    } else {
    	context.globalCompositeOperation = "destination-over";   	
    }	
	context.arc(x, y, radius, 0, 2 * Math.PI);
	if (backgroundColor) {
	    context.fillStyle = backgroundColor;
	    context.fill();
	}
    if (border) {
    	context.lineWidth = borderSize;
	    context.strokeStyle = borderColor;
    }
    context.stroke();
    context.closePath();     	
}

function drawRect(x, y, width, height, backgroundColor, border, borderSize, borderColor, srcOver) {
	context.beginPath();
    if (srcOver) {
    	context.globalCompositeOperation = "source-over";  	
    } else {
    	context.globalCompositeOperation = "destination-over";   	
    }		
	if (border){
		context.rect(x, y, width, height);
		context.lineWidth = borderSize;
		context.strokeStyle = borderColor;
	} 
    if (backgroundColor) {
    	context.fillStyle = backgroundColor;
		context.fill();
    }	
	context.stroke();
	context.closePath();
}

function drawTrapezoid(fromX, fromY, toX, toY, fromHeight, toHeight, backgroundColor, border, borderSize, borderColor, srcOver) {
	context.beginPath();
    if (srcOver) {
    	context.globalCompositeOperation = "source-over";   	
    } else {
    	context.globalCompositeOperation = "destination-over";   	
    }
	context.moveTo(fromX, fromY);
	context.lineTo(toX, toY);
	context.lineTo(toX, toY+toHeight);
	context.lineTo(fromX, fromY+fromHeight);
	context.lineTo(fromX, fromY);
	if (backgroundColor) {
		context.fillStyle = backgroundColor;
		context.fill();
	}
	context.lineWidth = borderSize;
	context.strokeStyle = borderColor;
	context.stroke();			
	context.closePath();
}