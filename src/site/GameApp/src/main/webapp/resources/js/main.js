// Constants
var timeout = 50000;

// Variables
var gameName;
var level;
var mapCol;
var mapRow;
var mapTypes = [];
var mapCollisionTypes = [];
var playerColors = [];
var map = [[]];
var players = [];
var player;
var selectedPlayer;
var creatures = [];
var spawnPoints = [];
var playerId;
var playerName;
var userId;
var handleKeys = false;
var viewingGames = false;
var viewingView = false;
var viewingPlayer = false;
var completedGame = false;
var playerMoved = false;
var processing = false;
var selectedCreatureId = null;
var selectedSpawnPointId = null;
var selectedPlayerId = null;
var remainingTime = null;
var hitPoints = null;
var attack = null;
var defense = null;
var range = null;
var damage = null;

// Classes

// Events

// Functions
function createSession() {
	console.log("createSession");
	if (!error){
		connect(open, closed);
	}	
}

function open() {	
	console.log("open");
	if (!userId) {
		login();
	} 
}

function closed() {
	console.log("closed");
	userId = null;
	websocket = null;
	login();
}

function login() {
	console.log("login");
	if (!websocket) {
		createSession();
	} else {	
		let request = [];
		userId = $("#userId").val();
		request[0] = userId;
		request[1] = $("#password").val();
		message = createMessage("actions.LoginAction","login",request);
		send(message);		
	}
}

function getGames(){
	console.log("getGames");
	let request = [];
	message = createMessage("actions.GameAction","getGames",request);
	send(message);		
}

function getPlayer(id) {
	let results = null;
	for (let player of players) {
		if (player) {
			if (player.playerId == id) {
				results = player;
				break;
			}
		}
	}
	return results;
}

function getCreature(id) {
	let results = null;
	for (let creature of creatures) {
		if (creature.id == id) {
			results = creature;
			break;
		}
	}
	return results;
}

function getSpawnPoint(id) {
	let results = null;
	for (let spawnPoint of spawnPoints) {
		if (spawnPoint.id == id) {
			results = spawnPoint;
			break;
		}
	}
	return results;
}

function exitGame() {
	console.log("exitGame");
	let request = [];
	request[0] = gameName;
	message = createMessage("actions.GameAction","exitGame",request);
	send(message);	
	gameName = null;
	playerId = null;
	playerName = null;
	viewingGames = true;
	viewingView = false;
}

function joinGame(gameName){
	console.log("joinGame: ",gameName);
	playerName = $('#playerName').val();
	if (playerName) {
		viewingPlayer = false;
		let request = [];
		request[0] = gameName;
		request[1] = playerName;
		message = createMessage("actions.GameAction","joinGame",request);
		send(message);
	} else {
		createPlayer(gameName);
	}
}

function createPlayer(gameName) {
	console.log("createPlayer:",gameName);
	let html = "";
	viewingPlayer = true;
	html+="<div class='title'>Create Player</div>";
	html+="<table>";
	html+="<tr>";
	html+="<td>";
	html+="<button onclick='joinGame(\""+gameName+"\")'>Submit</button>";
	html+="<button onclick='viewingPlayer = false;showGames()'>Cancel</button>";
	html+="</td>";
	html+="</tr>";	
	html+="</table>";
	html+="<table class='center'>";
	html+="<tr>";
	html+="<td class='label' style='width:30%'>Name:</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='playerName' maxlength='20' size='20'></input>";
	html+="</td>";
	html+="</tr>";
	html+="</table>";
	$("#viewPlayerDiv").html(html);
	$("#gamesDiv").hide();
	$("#canvasDiv").hide();
	$("#playerInfoDiv").hide();
	$("#viewPlayerDiv").show();	
}

function updatePlayer() {
	console.log("updatePlayer");
	player.maxHitPoints = parseInt($("#hitPoints").val());
	player.experience = parseInt($("#experience").val());
	player.attack = parseInt($("#attack").val());
	player.defense = parseInt($("#defense").val());
	player.range = parseInt($("#range").val());
	player.damage = parseInt($("#damage").val());
	let request = [];
	request[0] = gameName;
	request[1] = player;
	message = createMessage("actions.GameAction","updatePlayer",request);
	send(message);		
}

function viewPlayer() {
	console.log("viewPlayer:");
	let html = "";
	viewingPlayer = true;
	html+="<div class='title'>Update Player</div>";
	html+="<table>";
	html+="<tr>";
	html+="<td>";
	html+="<button onclick='viewingPlayer = false;updatePlayer()'>Submit</button>";
	html+="<button onclick='viewingPlayer = false;drawView()'>Cancel</button>";
	html+="</td>";
	html+="</tr>";	
	html+="</table>";
	html+="<table class='center' style='width:40%'>";
	html+="<tr>";
	html+="<td class='label' style='width:30%'>Name:</td>";	
	html+="<td style='text-align:left'>";
	html+=player.name;
	html+="</td>";
	html+="</tr>";
	html+="<tr>";
	html+="<td class='label'>Experience:</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='experience' disabled='true'></input>";
	html+="</td>";
	html+="</tr>";	
	html+="<tr>";
	html+="<td class='label'>Hit Points (2 XP):</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='hitPoints' type='number' min='"+player.maxHitPoints+"' onchange='changeHitPoints()'></input>";
	html+="</td>";
	html+="</tr>";	
	html+="<tr>";
	html+="<td class='label'>Attack (10 XP):</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='attack' type='number' min='"+player.attack+"' onchange='changeAttack()'></input>";
	html+="</td>";
	html+="</tr>";	
	html+="<tr>";
	html+="<td class='label'>Defense (10 XP):</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='defense' type='number' min='"+player.defense+"' onchange='changeDefense()'></input>";
	html+="</td>";
	html+="</tr>";
	html+="<tr>";
	html+="<td class='label'>Range (10 XP):</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='range' type='number' min='"+player.range+"' onchange='changeRange()'></input>";
	html+="</td>";
	html+="</tr>";			
	html+="<tr>";	
	html+="<td class='label'>Damage (20 XP):</td>";	
	html+="<td style='text-align:left'>";
	html+="<input id='damage' type='number' min='"+player.damage+"' onchange='changeDamage()'></input>";
	html+="</td>";
	html+="</tr>";		
	html+="</table>";
	$("#viewPlayerDiv").html(html);
	$("#experience").val(parseInt(player.experience));
	$("#hitPoints").val(parseInt(player.maxHitPoints));
	$("#attack").val(parseInt(player.attack));
	$("#defense").val(parseInt(player.defense));
	$("#range").val(parseInt(player.range));
	$("#damage").val(parseInt(player.damage));
	hitPoints = parseInt(player.maxHitPoints);
	attack = parseInt(player.attack);
	defense = parseInt(player.defense);
	range = parseInt(player.range);
	damage = parseInt(player.damage);
	$("#gamesDiv").hide();
	$("#canvasDiv").hide();
	$("#playerInfoDiv").hide();
	$("#viewPlayerDiv").show();	
}

function changeHitPoints() {
	if ($("#experience").val() >= 2) {
		if ($("#hitPoints").val() > hitPoints) {
			$("#experience").val(+$("#experience").val() - 2);
		} 
		if ($("#hitPoints").val() <= hitPoints) {
			$("#experience").val(+$("#experience").val() + 2);
		}
		hitPoints = $("#hitPoints").val();
	} else {
		if ($("#hitPoints").val() <= hitPoints) {
			$("#experience").val(+$("#experience").val() + 2);
			hitPoints = $("#hitPoints").val();
		} else {		
			$("#hitPoints").val(hitPoints);
		}
	}
}

function changeAttack() {
	if ($("#experience").val() >= 10) {
		if ($("#attack").val() > attack) {
			$("#experience").val(+$("#experience").val() - 10);
		} 
		if ($("#attack").val() <= attack) {
			$("#experience").val(+$("#experience").val() + 10);
		}
		attack = $("#attack").val();
	} else {
		if ($("#attack").val() <= attack) {
			$("#experience").val(+$("#experience").val() + 10);
			attack = $("#attack").val();
		} else {		
			$("#attack").val(attack);
		}
	}
}

function changeDefense() {
	if ($("#experience").val() >= 10) {
		if ($("#defense").val() > defense) {
			$("#experience").val(+$("#experience").val() - 10);
		} 
		if ($("#defense").val() <= defense) {
			$("#experience").val(+$("#experience").val() + 10);
		}
		defense = $("#defense").val();
	} else {
		if ($("#defense").val() <= defense) {
			$("#experience").val(+$("#experience").val() + 10);
			defense = $("#defense").val();
		} else {		
			$("#defense").val(defense);
		}
	}
}

function changeRange() {
	if ($("#experience").val() >= 10) {
		if ($("#range").val() > range) {
			$("#experience").val(+$("#experience").val() - 10);
		} 
		if ($("#range").val() <= range) {
			$("#experience").val(+$("#experience").val() + 10);
		}
		range = $("#range").val();
	} else {
		if ($("#range").val() <= range) {
			$("#experience").val(+$("#experience").val() + 10);
			range = $("#range").val();
		} else {		
			$("#range").val(range);
		}
	}
}

function changeDamage() {
	if ($("#experience").val() >= 20) {
		if ($("#damage").val() > damage) {
			$("#experience").val(+$("#experience").val() - 20);
		} 
		if ($("#damage").val() <= damage) {
			$("#experience").val(+$("#experience").val() + 20);
		}
		damage = $("#damage").val();
	} else {
		if ($("#damage").val() <= damage) {
			$("#experience").val(+$("#experience").val() + 20);
			damage = $("#damage").val();
		} else {		
			$("#damage").val(damage);
		}
	}
}

function selectTarget() {
	if (selectedCreatureId) {
		let request = [];
		request[0] = gameName;
		request[1] = selectedCreatureId;
		message = createMessage("actions.GameAction","selectCreature",request);
		send(message);
	}
	if (selectedSpawnPointId) {
		let request = [];
		request[0] = gameName;
		request[1] = selectedSpawnPointId;
		message = createMessage("actions.GameAction","selectSpawnPoint",request);
		send(message);
	}
}

function movePlayer(direction) { // 1=up, 2=right, 3=down, 4=left
	console.log("movePlayer");
	if (!playerMoved) {
		playerMoved = true;
		let request = [];
		request[0] = gameName;
		request[1] = parseInt(direction);
		message = createMessage("actions.GameAction","movePlayer",request);
		send(message);
	}
}

function createGame(){
	console.log("createGame");
	let request = [];
	message = createMessage("actions.GameAction","createGame",request);
	send(message);		
}

function deleteGame(name){
	console.log("deleteGame: ",name);
	$("#completedGameDiv").hide();
	let request = [];
	request[0] = name;
	message = createMessage("actions.GameAction","deleteGame",request);
	send(message);		
}

function handleKeyDown(e){
	console.log("handleKeyDown:",e.code || e.keyCode);
	if (handleKeys) {
		console.log("Handle Key");
		switch(e.code || e.keyCode) {
			case 'KeyW': handleKeys=false;setTimeout(()=>{movePlayer(1);handleKeys=true},100);break;
			case 87: handleKeys=false;setTimeout(()=>{movePlayer(1);handleKeys=true},100);break;
			case 'KeyE': handleKeys=false;setTimeout(()=>{movePlayer(2);handleKeys=true},100);break;
			case 69: handleKeys=false;setTimeout(()=>{movePlayer(2);handleKeys=true},100);break;
			case 'KeyD': handleKeys=false;setTimeout(()=>{movePlayer(3);handleKeys=true},100);break;
			case 68: handleKeys=false;setTimeout(()=>{movePlayer(3);handleKeys=true},100);break;
			case 'KeyC': handleKeys=false;setTimeout(()=>{movePlayer(4);handleKeys=true},100);break;
			case 67: handleKeys=false;setTimeout(()=>{movePlayer(4);handleKeys=true},100);break;
			case 'KeyX': handleKeys=false;setTimeout(()=>{movePlayer(5);handleKeys=true},100);break;
			case 88: handleKeys=false;setTimeout(()=>{movePlayer(5);handleKeys=true},100);break;
			case 'KeyZ': handleKeys=false;setTimeout(()=>{movePlayer(6);handleKeys=true},100);break;
			case 90: handleKeys=false;setTimeout(()=>{movePlayer(6);handleKeys=true},100);break;
			case 'KeyA': handleKeys=false;setTimeout(()=>{movePlayer(7);handleKeys=true},100);break;
			case 65: handleKeys=false;setTimeout(()=>{movePlayer(7);handleKeys=true},100);break;
			case 'KeyQ': handleKeys=false;setTimeout(()=>{movePlayer(8);handleKeys=true},100);break;
			case 81: handleKeys=false;setTimeout(()=>{movePlayer(8);handleKeys=true},100);break;
		}
	}
}

function selectTile(id) {
	console.log("selectTile:",id);
	selectedPlayerId = null;
	selectedCreatureId = null;
	selectedSpawnPointId = null;		
	let tile = tiles.get(id);
	if (id.startsWith("P")) {
		if (playerId != id.substring(1)) {
			selectedPlayerId = id.substring(1);	
			selectedCreatureId = null;
			selectedSpawnPointId = null;
		}
	} 
	if (id.startsWith("C")) {
		selectedCreatureId = id.substring(1);
		selectedSpawnPointId = null;
		selectTarget();
	} 
	if (id.startsWith("S")) {
		selectedSpawnPointId = id.substring(1);
		selectedCreatureId = null;
		selectTarget();
	} 
}

function drawView(){
	console.log("drawView");
	let tileSize = 25;
	let locationArray = player.location.split(",");
	let viewXMin = locationArray[0]-Math.floor((canvas.width/tileSize)/2);
	let viewYMin = locationArray[1]-Math.floor((canvas.height/tileSize)/2);
	let viewXMax = null;
	let viewYMax = null;
	tiles = new Map();
	buttons = new Map();	
	handleKeys = true;
	viewingGames = false;
	viewingView = true;
	let html = "";
	html+="<div style='position:fixed;margin-top:0px;background:black'>";
	html+="<label class='label'>Remaining Time:</label>";
	html+="<span class='border' style='padding:5px'>"+remainingTime+"</span>";
	html+="<label class='bold' style='font-size:16px;margin-left:20px'>Level: "+level+"</label>";
	html+="<span class='title' style='margin-left:30px'>LEVEL</span>";
	html+="<hr>";
	html+="<label class='label' style='margin-left:10px'>Name:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.name+"</span>";
	html+="<label class='label' style='margin-left:10px'>HP:</label><span class='border' style='border-color:"+(player.hitPoints < player.maxHitPoints?"red":"white")+";padding-left:10px;padding-right:10px'>"+player.hitPoints+"</span>";
	html+="<label class='label' style='margin-left:10px'>Max HP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.maxHitPoints+"</span>";
	html+="<label class='label' style='margin-left:10px'>XP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.experience+"</span>";
	html+="<label class='label' style='margin-left:10px'>Range:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.range+"</span>";
	html+="<label class='label' style='margin-left:10px'>Attack:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.attack+"</span>";
	html+="<label class='label' style='margin-left:10px'>Defense:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.defense+"</span>";	
	html+="<label class='label' style='margin-left:10px'>Damage:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+player.damage+"</span>";
	html+="<span style='padding-left:10px'></span>";
	if (player.location == player.startingLocation) {
		html+="<button style='margin-left:10px' onclick='viewPlayer()'>Update</button>";
	}
	if (player.message) {
		html+="<span style='margin-left:10px;background:white;color:black;padding:5px'>"+player.message+"</span>";
	}
	html+="<hr>";
	if (selectedCreatureId) {
		let selectedCreature = getCreature(selectedCreatureId);
		if (selectedCreature) {
			html+="<label class='label' style='margin-left:10px'>Creature:</label>";
			html+="<label class='label' style='margin-left:10px'>ID:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.id+"</span>";
			html+="<label class='label' style='margin-left:10px'>HP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.hitPoints+"</span>";	
			html+="<label class='label' style='margin-left:10px'>Range:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.range+"</span>";
			html+="<label class='label' style='margin-left:10px'>Attack:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.attack+"</span>";
			html+="<label class='label' style='margin-left:10px'>Defense:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.defense+"</span>";
			html+="<label class='label' style='margin-left:10px'>Damage:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedCreature.damage+"</span>";
			if (selectedCreature.message) {
				html+="<span style='margin-left:10px;background:white;color:black;padding:5px'>"+selectedCreature.message+"</span>";
			}
			html+="<hr>";
		}
	}
	if (selectedSpawnPointId) {
		let selectedSpawnPoint = getSpawnPoint(selectedSpawnPointId);
		if (selectedSpawnPoint) {
			html+="<label class='label' style='margin-left:10px'>Spawn Point:</label>";
			html+="<label class='label' style='margin-left:10px'>ID:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedSpawnPoint.id+"</span>";
			html+="<label class='label' style='margin-left:10px'>HP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedSpawnPoint.hitPoints+"</span>";	
			if (selectedSpawnPoint.message) {
				html+="<span style='margin-left:10px;background:white;color:black;padding:5px'>"+selectedSpawnPoint.message+"</span>";
			}
			html+="<hr>";
		}
	}	
	if (selectedPlayerId) {
		selectedPlayer = getPlayer(selectedPlayerId);
		html+="<label class='label' style='margin-left:50px'>Player:</label>";
		html+="<label class='label' style='margin-left:10px'>Name:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.name+"</span>";
		html+="<label class='label' style='margin-left:10px'>HP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.hitPoints+"</span>";
		html+="<label class='label' style='margin-left:10px'>Max HP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.maxHitPoints+"</span>";
		html+="<label class='label' style='margin-left:10px'>XP:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.experience+"</span>";	
		html+="<label class='label' style='margin-left:10px'>Range:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.range+"</span>";
		html+="<label class='label' style='margin-left:10px'>Attack:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.attack+"</span>";
		html+="<label class='label' style='margin-left:10px'>Defense:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.defense+"</span>";
		html+="<label class='label' style='margin-left:10px'>Damage:</label><span class='border' style='border-color:white;padding-left:10px;padding-right:10px'>"+selectedPlayer.damage+"</span>";
		html+="<hr>";
	}	
	html+="</div>";
	$("#playerDiv").html(html);	
	$("#gamesDiv").hide();	
	$("#completedLevelDiv").hide();
	$("#failedLevelDiv").hide();
	if (!viewingPlayer) {
		$("#viewPlayerDiv").hide();
		$("#playerDiv").show();
		$("#canvasDiv").show();
	}
	context.clearRect(0, 0, canvas.width, canvas.height);
	// Map
	drawRect(0, 0, canvas.width, canvas.height, "black", false, null, null, false);
	for (r = 0;r < Math.floor(canvas.height/tileSize);r++) {
		for (c = 0;c < Math.floor(canvas.width/tileSize);c++) {
			let id = "M"+r+c;
			if (r*tileSize+tileSize < canvas.height &&
				c*tileSize+tileSize < canvas.width) {
				viewXMax = viewXMin+c;
				viewYMax = viewYMin+r;
				if (viewXMin+c >= 0 &&
					viewYMin+r >= 0 &&
					viewXMin+c < mapCols &&
					viewYMin+r < mapRows) {
					mapType = mapTypes[map[viewXMin+c][viewYMin+r]];
					drawTile(id,null,mapType,"black","black",2,c*tileSize,r*tileSize,tileSize,tileSize,function() { selectTile(id) });
				} else {
					drawTile(id,null,"black","black","black",2,c*tileSize,r*tileSize,tileSize,tileSize,function() { selectTile(id) });
				}
			} else {
				drawTile(id,null,"black","black","black",2,c*tileSize,r*tileSize,tileSize,tileSize,function() { selectTile(id) });
				break;
			} 
		} 
	}
	// Players
	for (let player of players) {
		if (player) {
			playerLocation = player.location.split(",");
			if (playerLocation[0] > viewXMin &&
				playerLocation[0] <= viewXMax &&
				playerLocation[1] > viewYMin &&
				playerLocation[1] <= viewYMax) {
				let id = "P"+player.playerId;
				drawTile(id,null,playerColors[player.playerId],"black","red",2,(playerLocation[0]-viewXMin)*tileSize,(playerLocation[1]-viewYMin)*tileSize,tileSize,tileSize,function(){ selectTile(id) });
				if (selectedPlayerId &&
					selectedPlayerId == player.id) {
					let tile = tiles.get(id);
					tile.selected = true;
					tile.draw();
				}				
				switch(player.direction) {
					case(1):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  (((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2))-Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;
					case(2):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  (((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2))+Math.floor(tileSize/2),
							  (((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2))-Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(3):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(4):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(5):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(6):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(7):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;	
					case(8):drawLine(
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((playerLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((playerLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  2,
							  "black",
							  true
							);break;							
				}
			}
		}
	}
	// SpawnPoints
	if (spawnPoints) {
		for (let spawnPoint of spawnPoints) {
			let spawnPointLocation = spawnPoint.location.split(",");		
			let id = "S"+spawnPoint.id;
			drawTile(id,null,spawnPoint.color,"gray","red",2,(spawnPointLocation[0]-viewXMin)*tileSize,(spawnPointLocation[1]-viewYMin)*tileSize,tileSize,tileSize,function(){ selectTile(id) });
			if (selectedSpawnPointId &&
				selectedSpawnPointId == spawnPoint.id) {
				let tile = tiles.get(id);
				tile.selected = true;
				tile.draw();
			}			
		}
	}
	// Creatures
	if (creatures) {
		for (let creature of creatures) {
			let creatureLocation = creature.location.split(",");
			if (creatureLocation[0] > viewXMin &&
				creatureLocation[0] <= viewXMax &&
				creatureLocation[1] > viewYMin &&
				creatureLocation[1] <= viewYMax) {
				let id = "C"+creature.id;
				drawTile(id,null,creature.color,"white","red",2,(creatureLocation[0]-viewXMin)*tileSize,(creatureLocation[1]-viewYMin)*tileSize,tileSize,tileSize,function(){ selectTile(id) });
				if (selectedCreatureId &&
					selectedCreatureId == creature.id) {
					let tile = tiles.get(id);
					tile.selected = true;
					tile.draw();
					
				}
				switch(creature.direction) {
					case(1):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  (((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2))-Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;
					case(2):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  (((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2))+Math.floor(tileSize/2),
							  (((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2))-Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(3):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(4):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(5):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(6):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)+Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(7):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;	
					case(8):drawLine(
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2),
							  ((creatureLocation[0]-viewXMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  ((creatureLocation[1]-viewYMin)*tileSize)+Math.floor(tileSize/2)-Math.floor(tileSize/2),
							  2,
							  "white",
							  true
							);break;							
				}
			}
		}
	}
}

function resizeScreen() {
	console.log('resizeScreen');
	if (viewingView) {
		drawView();	
	}
}

function showGames() {
	$("#viewPlayerDiv").hide();
	$("#canvasDiv").hide();
	$("#completedGameDiv").hide();
	$("#gamesDiv").show();
}
	
// Call Back Functions
function loginResponse(parameters) {
	console.log("loginResponse: ", parameters);
	let status = parameters[0];
	if (status === "Success"){
		$("#loginDiv").hide();
		getGames();
	}
}

function displayLogin(parameters) {
	console.log("displayLogin: ", parameters);
	$("#canvasDiv").hide();	
	$("#gamesDiv").hide();
	$("#viewPlayerDiv").hide();
	$("#playerInfoDiv").hide();
	$("#loginDiv").show();
}

function displayGames(parameters){
	console.log("displayGames: ", parameters);
	processing = false;
	let games = parameters[0];
	let html = "";
	viewingGames = true;
	viewingView = false;
	html+="<table style='width:100%' border='1'>";
	html+="<tr>";
	html+="<th width='10%'>Actions</th>";
	html+="<th width='20%'>Name</th>";
	html+="<th width='20%'>Created By</th>";
	html+="<th width='20%'># Players</th>";
	html+="<th width='20%'>Max Players</th>";
	html+="</tr>";
	for (idx = 0;idx < games.length;idx++){
		html+="<tr>";
		html+="<td>";
		if (games[idx][2] != games[idx][3]) {
			html+="<button onclick='joinGame(\""+games[idx][0]+"\")'>Join</button>";
		}
		if (parameters[0][idx][1] === userId) {
			html+="<button onclick='deleteGame(\""+games[idx][0]+"\")'>Delete</button></td>";
		}
		html+="<td>"+games[idx][0]+"</td>";
		html+="<td>"+games[idx][1]+"</td>";
		html+="<td>"+games[idx][2]+"</td>";
		html+="<td>"+games[idx][3]+"</td>";
		html+="</tr>";
	}
	html+="</table>";
	$("#gameListDiv").html(html);
	$("#canvasDiv").hide();
	if (!viewingPlayer) {
		$("#viewPlayerDiv").hide();
		$("#gamesDiv").show();	
		$("#gamesListDiv").show();
	}
}

function initGame(parameters) {
	console.log("initGame: ", parameters);
	gameName = parameters[0];
	level = parameters[1];
	mapTypes = parameters[2];	
	mapCollisionTypes = parameters[3];
	mapRows = parameters[4];
	mapCols = parameters[5];
	map = parameters[6];
	playerColors = parameters[7];
	players = parameters[8];
	playerId = parameters[9];
	spawnPoints = parameters[10];
	player = getPlayer(playerId);
	drawView();
}

function refreshGame(parameters) {
	console.log("refreshGame: ", parameters);
	processing = false;
	players = parameters[0];
	creatures = parameters[1];
	spawnPoints = parameters[2];
	remainingTime = parameters[3];
	completedGame = parameters[4];
	completedLevel = parameters[5];
	failedLevel = parameters[6];
	player = getPlayer(playerId);
	playerMoved = false;
	if (completedGame) {
		$("#canvasDiv").hide();
		$("#completedLevelDiv").hide();
		$("#failedLevelDiv").hide();
		let html = "";
		html+="<h1 class='align-center'>Game Completed</h1>";
		html+="<hr>";
		html+="<button onclick='deleteGame(\""+gameName+"\")'>Exit</button>";
		$("#completedGameDiv").html(html);
		$("#completedGameDiv").show();
		return;
	}	
	if (completedLevel) {
		$("#canvasDiv").hide();
		$("#completedLevelMessage").html("Level "+level+" Completed");
		$("#completedLevelDiv").show();
		return;
	}
	if (failedLevel) {
		$("#canvasDiv").hide();
		$("#failedLevelMessage").html("Level "+level+" Failed");
		$("#failedLevelDiv").show();
		return;
	}
	if (viewingView) {
		drawView();	
	}
}

function nextLevel() {
	console.log("nextLevel");
	if (!processing) {
		processing = true;
		let request = [];
		request[0] = gameName;
		message = createMessage("actions.GameAction","nextLevel",request);
		send(message);
	}
}