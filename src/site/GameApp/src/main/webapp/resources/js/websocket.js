var wsUri = (window.location.protocol === "https:"?"wss://":"ws://")+window.location.hostname+(window.location.port?":"+window.location.port:"")+window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/'))+"/actions"
var websocket;
var webSocketMessage = document.getElementById("webSocketMessage");
var error = false;
var timerId;
var keepAlive = false;

function onError(evt) {
	error = true;
	if (webSocketMessage) {
		if (evt &&
			evt.data) {
			webSocketMessage.innerHTML = "<span style='color: red;'>ERROR: "+evt.data+"</span> ";
		} else {
			webSocketMessage.innerHTML = "<scan style='color: red;'>ERROR: lost connection</span> ";
		}
	} else {
		if (evt &&
			evt.data) {
			alert(evt.data);
		} else {
			alert("Lost Connection");
			cancelKeepAlive();
		}
	}
	location.replace((window.location.protocol === "https:"?"https://":"http://")+window.location.hostname+(window.location.port?":"+window.location.port:"")+window.location.pathname.substring(0,window.location.pathname.lastIndexOf('/')));
}

function onOpen(evt, openCallback) {
	console.log("Open connection: "+new Date());
    openCallback();
    activateKeepAlive();
}

function onClose(evt, closeCallback) {
	console.log("Close connection: "+new Date()+": "+evt.code);
    closeCallback();
    cancelKeepAlive();
}

function onMessage(evt) {
	if (evt &&
		evt.data) {
		var inputMessage = JSON.parse(evt.data);
		if (inputMessage.parameters){
			window[inputMessage.methodName](inputMessage.parameters);
		} else {
			window[inputMessage.methodName]();
		}
	} else {
		alert("Lost Connection");
	}
}

function connect(openCallback, closeCallback) {
	console.log("Calling connect: "+new Date());
	if ('WebSocket' in window) {	
		websocket = new WebSocket(wsUri);
	} 
	if ('MozWebSocket' in window) {
		websocket = new MozWebSocket(wsUri);		
	}
	if (websocket) {
		websocket.onerror = function(evt) { onError(evt); }
		websocket.onopen = function(evt) { onOpen(evt, openCallback); }
		websocket.onclose = function(evt) { onClose(evt, closeCallback); }
		websocket.onmessage = function(evt) { onMessage(evt); }	
	} else {
		if (webSocketMessage){
			webSocketMessage.innerHTML = "WebSockets not supported! ";
		} else {		
			alert('WebSockets not supported!');
		}
	}
}

function close() {
	if (websocket){
		websocket.close();
	}
}

function send(message) {	
	console.log("Sending data: "+new Date());
	if (websocket && websocket.readyState == 1){
		websocket.send(message);
	} else {
		if (webSocketMessage){
			webSocketMessage.innerHTML = "Connection not open. ";
		} else {
			alert("Connection not open.");
		}
	}
}

function createMessage(className, methodName, parameters){
	var message;
	message = "{ \"className\":\""+className+"\", \"methodName\":\""+methodName+"\", \"parameters\":";
	message += JSON.stringify(parameters);
	message += " }";
	return message;
} 

function displayMessageError(parameters){
	if (webSocketMessage){
		webSocketMessage.innerHTML = parameters[0];
	} else {
		alert(parameters[0]);
	}
}

function activateKeepAlive() {
	console.log("Calling activateKeepAlive: "+new Date());
	keepAlive = true;
	if (timeout){
		timerId = setTimeout(ping, timeout);
	} else {
		timerId = setTimeout(ping, 60000);
	}
}

function ping() {
	console.log("Send ping: "+new Date());
	var message;
	var parameters = [];
	message = createMessage("websocketserver.actions.SystemAction","ping",parameters);
	send(message);
	if (keepAlive) {
		activateKeepAlive();
	}
}

function cancelKeepAlive() {
	console.log("Calling cancelKeepAlive: "+new Date());
	clearTimeout(timerId);
	keepAlive = false;
}

function pong(parameters) {
	console.log("Received pong: "+new Date());
}

