package utilities;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import beans.Creature;
import beans.Game;
import beans.Play;
import beans.Player;
import beans.SpawnPoint;

public class CommonUtility {
	
	public CommonUtility() {}

	public static synchronized String getEnvironmentEntry(String name) throws NamingException {
		String value = null;
		InitialContext ic = new InitialContext();
		value = (String) ic.lookup("java:comp/env/"+name);
		return value;
	}
	
	public static synchronized Boolean checkCollision(String gameName, String location[], Boolean monster) {
		Boolean collision = false;
		Game game = Play.games.get(gameName);
		if (game != null) {
			// Collide with edge of map
			if (Integer.valueOf(location[0]) < 0 ||
				Integer.valueOf(location[0]) >= game.getData().getMapCols() ||
				Integer.valueOf(location[1]) < 0 ||
				Integer.valueOf(location[1]) >= game.getData().getMapRows()) {
				collision = true;
			}
			// Collide with map collision objects
			if (!collision) {
				String mapCollisionTypes[] = game.getData().getMapCollisionTypes();
				for (String collisionType:mapCollisionTypes) {
					if (game.getData().getMap()[Integer.valueOf(location[0])][Integer.valueOf(location[1])] == Integer.valueOf(collisionType)) {		
						collision = true;
						break;
					}			
				}	
			}
			// Monster collide with map collision objects
			if (monster &&
			    !collision) {
				String mapCollisionTypes[] = game.getData().getMapMonsterCollisionTypes();
				for (String collisionType:mapCollisionTypes) {
					if (game.getData().getMap()[Integer.valueOf(location[0])][Integer.valueOf(location[1])] == Integer.valueOf(collisionType)) {		
						collision = true;
						break;
					}			
				}	
			}			
			// Collide with another player
			if (!collision) {
				for (Player otherPlayer:game.getData().getPlayerList()) {
					if (otherPlayer != null) {
						String otherPlayerLocation[] = otherPlayer.getLocation().split(",");
						if (location[0].equals(otherPlayerLocation[0]) &&
							location[1].equals(otherPlayerLocation[1])) {		
							collision = true;
							break;
						}
					}
				}
			}
			// Collide with creature
			if (!collision) {
				for (Creature creature:game.getData().getCreatureList()) {
					if (creature != null) {
						String creatureLocation[] = creature.getLocation().split(",");
						if (location[0].equals(creatureLocation[0]) &&
							location[1].equals(creatureLocation[1])) {		
							collision = true;
							break;
						}
					}
				}				
			}
			// Collide with spawn point
			if (!collision) {
				for (SpawnPoint spawnPoint:game.getData().getSpawnPoints().values()) {
					if (spawnPoint != null) {
						String spawnPointLocation[] = spawnPoint.getLocation().split(",");
						if (location[0].equals(spawnPointLocation[0]) &&
							location[1].equals(spawnPointLocation[1])) {		
							collision = true;
							break;
						}
					}
				}				
			}			
		}
		return collision;
	}	
}
