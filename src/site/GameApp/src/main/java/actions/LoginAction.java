package actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import websocketserver.actions.Action;
import websocketserver.beans.Message;

public class LoginAction extends Action {
	
	public LoginAction() {}

	public Message login(List<Object> parameters) {
		List<Object> outParameters = new ArrayList<Object>();	
		Message responseMessage = null;
		String userId = (String)parameters.get(0);
		String password = (String)parameters.get(1);
		if (userId != null && !userId.isEmpty()) {
			//@ TODO Add logic to validate user
			getSession().getUserProperties().put("UserId", userId);
			outParameters.add("Success");
			responseMessage = new Message("LoginAction","loginResponse",outParameters);
		} else {	
			try { getSession().close(); } catch (IOException ioe) { outParameters.add(ioe.getMessage()); } 
			outParameters.add("Not Authorized");			
			responseMessage = new Message("LoginAction","displayMessageError",outParameters);
		}
		return responseMessage;
	}
}
