package actions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.websocket.Session;

import beans.Game;
import beans.Play;
import beans.Player;
import exceptions.InvalidRequestException;
import utilities.CommonUtility;
import websocketserver.actions.Action;
import websocketserver.beans.Message;

public class GameAction extends Action {
	
	public void publishGame(String gameName) throws Exception {
		if (gameName != null) {
			Game game = Play.games.get(gameName);	
			if (game != null) {
				List<Object> outParameters = new ArrayList<Object>();
				outParameters.add(game.getData().getPlayerList());
				outParameters.add(game.getData().getCreatureList());
				outParameters.add(game.getData().getSpawnPointList());
				outParameters.add(game.getData().getRemainingTime());
				outParameters.add(game.getData().getCompletedGame());
				outParameters.add(game.getData().getCompletedLevel());
				outParameters.add(game.getData().getFailedLevel());
				Message responseMessage = new Message("GameAction","refreshGame",outParameters);				
				for (Session session:getWebSocketServer().getSessions().values()) {
					if (session != null &&
						session.isOpen()) {
						String userGameName = (String)session.getUserProperties().get("GameName");
						if (userGameName != null &&
							userGameName.equals(gameName) &&
							session.getUserProperties().get("UserId") != null) {
							sendMessage(session, responseMessage);
						}
					} else {
						List<Object> parameters = new ArrayList<Object>();
						parameters.add(game.getData().getName());
						exitGame(parameters);
						getWebSocketServer().getSessions().remove(session.getUserProperties().get("UserId"));
					}
				}
			}
		}
	}

	public void publishGameList() throws Exception {
		Message responseMessage = getGames(null);
		for (Session session:getWebSocketServer().getSessions().values()) {
			if (session != null &&
				session.isOpen()) {
				if (session.getUserProperties().get("UserId") != null &&
					Play.games.get(session.getUserProperties().get("GameName")) == null) {
					sendMessage(session, responseMessage);
				}
			} else {
				getWebSocketServer().getSessions().remove(session.getUserProperties().get("UserId"));
			}
		}
	}	

	public Message createGame(List<Object> parameters) throws InvalidRequestException, Exception {
		Message responseMessage = null;
		List<Object> outParameters = null;	
		if (getSession().getUserProperties().get("UserId") != null) {
			Game game = new Game(new Long(System.currentTimeMillis()).toString(),1,(String)getSession().getUserProperties().get("UserId"), this);
			Play.games.put(game.getData().getName(), game);	
			game.start();
			publishGameList();
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);	
		}
		return responseMessage;		
	}
	
	public Message exitGame(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		Game game = Play.games.get(parameters.get(0));
		if (game != null) {
			Player player = game.getData().getPlayers().get((String)getSession().getUserProperties().get("UserId"));
			game.getData().getPlayerIds()[Integer.valueOf(player.getPlayerId())] = false;
			game.getData().getPlayers().remove(player.getUserId());
			getSession().getUserProperties().remove("GameName");
		}
		publishGameList();
		publishGame(game.getData().getName());
		return responseMessage;		
	}	
	
	public Message deleteGame(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		List<Object> outParameters = new ArrayList<Object>();
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {			
			String gameName = (String)parameters.get(0);
			Game game = Play.games.get(gameName);
			game.delete();
			Play.games.remove(gameName);
			publishGameList();
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);			
		}
		return responseMessage;		
	}
	
	public Message movePlayer(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {			
			String gameName = (String)parameters.get(0);
			Integer direction = (Integer)parameters.get(1);	
			Game game = Play.games.get(gameName);	
			Player player = game.getData().getPlayers().get(getSession().getUserProperties().get("UserId"));
			player.setDirection(direction);
			String locationArray[] = player.getLocation().split(",");
			switch(direction) {
				case 1: locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;
				case 2: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;
				case 3: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);break;
				case 4: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
				case 5: locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
				case 6: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
				case 7: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);break;
				case 8: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;				
			}	
			if (!CommonUtility.checkCollision(gameName, locationArray, false)) {
				player.setLocation(locationArray[0]+","+locationArray[1]);	
			}
			publishGame(gameName);
		} else {
			responseMessage = new Message("GameAction","displayLogin", null);				
		}
		return responseMessage;
	}
	
	public Message selectCreature(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {			
			String gameName = (String)parameters.get(0);
			Integer creatureId = Integer.valueOf((String)parameters.get(1));	
			Game game = Play.games.get(gameName);	
			Player player = game.getData().getPlayers().get(getSession().getUserProperties().get("UserId"));
			player.setSelectedCreatureId(creatureId);
			player.setSelectedSpawnPointId(null);
		} else {
			responseMessage = new Message("GameAction","displayLogin", null);				
		}
		return responseMessage;		
	}
	
	public Message selectSpawnPoint(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {			
			String gameName = (String)parameters.get(0);
			Integer spawnPointId = Integer.valueOf((String)parameters.get(1));	
			Game game = Play.games.get(gameName);	
			Player player = game.getData().getPlayers().get(getSession().getUserProperties().get("UserId"));
			player.setSelectedSpawnPointId(spawnPointId);
			player.setSelectedCreatureId(null);
		} else {
			responseMessage = new Message("GameAction","displayLogin", null);				
		}
		return responseMessage;		
	}	
	
	public Message updatePlayer(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		List<Object> outParameters = new ArrayList<Object>();
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {		
			String gameName = (String)parameters.get(0);
			LinkedHashMap playerData = (LinkedHashMap)parameters.get(1);
			Game game = Play.games.get(gameName);
			Player player = game.getData().getPlayerList()[(Integer)playerData.get("playerId")];
			player.setExperience((Integer)playerData.get("experience"));
			player.setMaxHitPoints((Integer)playerData.get("maxHitPoints"));
			player.setRange((Integer)playerData.get("range"));
			player.setAttack((Integer)playerData.get("attack"));
			player.setDefense((Integer)playerData.get("defense"));
			player.setDamage((Integer)playerData.get("damage"));
			publishGame(gameName);
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);			
		}
		return responseMessage;			
	}
	
	public Message joinGame(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		List<Object> outParameters = new ArrayList<Object>();
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {			
			String gameName = (String)parameters.get(0);
			String playerName = (String)parameters.get(1);
			Game game = Play.games.get(gameName);
			String userId = (String)getSession().getUserProperties().get("UserId");
			Boolean createNewPlayer = true;
			Player player = game.getData().getPlayers().get(userId);
			if (player != null) {
				createNewPlayer = false;
			}	
			if (createNewPlayer) {
				Integer playerId = game.getData().getAvailPlayerId();
				String playerLocation = game.getData().getProperties().get("locationPlayer"+(playerId));
				player = new Player(userId, playerId, playerLocation, 5, playerName, Integer.valueOf(game.getData().getProperties().get("playerExperience")), Integer.valueOf(game.getData().getProperties().get("playerMaxHitPoints")), Integer.valueOf(game.getData().getProperties().get("playerMaxHitPoints")), Integer.valueOf(game.getData().getProperties().get("playerRange")), Integer.valueOf(game.getData().getProperties().get("playerAttack")), Integer.valueOf(game.getData().getProperties().get("playerDefense")), Integer.valueOf(game.getData().getProperties().get("playerDamage")));
				getSession().getUserProperties().put("GameName", gameName);
				game.getData().addPlayer(player);
			}			
			outParameters.add(game.getData().getName());	
			outParameters.add(game.getData().getLevel());
			outParameters.add(game.getData().getMapTypes());
			outParameters.add(game.getData().getMapCollisionTypes());
			outParameters.add(game.getData().getMapRows());
			outParameters.add(game.getData().getMapCols());
			outParameters.add(game.getData().getMap());
			outParameters.add(game.getData().getPlayerColors());
			outParameters.add(game.getData().getPlayerList());
			outParameters.add(player.getPlayerId());
			outParameters.add(game.getData().getSpawnPointList());
			responseMessage = new Message("GameAction","initGame",outParameters);	
			publishGameList();
			publishGame(gameName);
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);			
		}
		return responseMessage;		
	}	
	
	public Message getGames(List<Object> parameters){
		Message responseMessage = null;
		List<Object> outParameters = new ArrayList<Object>();
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {	
			List<String> gameNameList = new ArrayList<String>(Play.games.keySet());
			List<List<Object>> gameList = new ArrayList<List<Object>>();
			for (String name:gameNameList) {
				Game game = Play.games.get(name);
				List<Object> gameData = new ArrayList<Object>();
				gameData.add(game.getData().getName());
				gameData.add(game.getData().getCreatedBy());
				gameData.add(String.valueOf(game.getData().getPlayerCount()));
				gameData.add(String.valueOf(game.getData().getNumberOfPlayers()));
				gameData.add(game.getData().getPlayerList());
				gameList.add(gameData);
			}
			outParameters.add(gameList);
			responseMessage = new Message("GameAction","displayGames",outParameters);
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);			
		}
		return responseMessage;
	}	
	
	public Message nextLevel(List<Object> parameters) throws Exception {
		Message responseMessage = null;
		List<Object> outParameters = new ArrayList<Object>();
		if (getSession() != null &&
			getSession().getUserProperties().get("UserId") != null) {		
			String gameName = (String)parameters.get(0);
			Game game = Play.games.get(gameName);
			game.nextLevel();
			for (Session session:getWebSocketServer().getSessions().values()) {
				if (session != null &&
					session.isOpen()) {
					String userGameName = (String)session.getUserProperties().get("GameName");
					if (userGameName != null &&
						userGameName.equals(gameName) &&
						session.getUserProperties().get("UserId") != null) {
						outParameters.add(game.getData().getName());	
						outParameters.add(game.getData().getLevel());
						outParameters.add(game.getData().getMapTypes());
						outParameters.add(game.getData().getMapCollisionTypes());
						outParameters.add(game.getData().getMapRows());
						outParameters.add(game.getData().getMapCols());
						outParameters.add(game.getData().getMap());
						outParameters.add(game.getData().getPlayerColors());
						outParameters.add(game.getData().getPlayerList());
						outParameters.add(game.getData().getPlayers().get(session.getUserProperties().get("UserId")).getPlayerId());
						outParameters.add(game.getData().getSpawnPoints());
						responseMessage = new Message("GameAction","initGame",outParameters);							
						sendMessage(session, responseMessage);
					}
				} else {
					outParameters.add(game.getData().getName());
					exitGame(outParameters);
					getWebSocketServer().getSessions().remove(session.getUserProperties().get("UserId"));
				}
			}
		} else {
			responseMessage = new Message("GameAction","displayLogin",outParameters);			
		}
		return responseMessage;			
	}	
}
