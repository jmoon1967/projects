package beans;

public class Player {

	private String userId;
	private Integer playerId;
	private String startingLocation;
	private String location;
	private Integer direction;
	private Integer actionTime;
	private Integer selectedCreatureId;
	private Integer selectedSpawnPointId;
	// Player's Info
	private String name;
	private Integer experience;
	private Integer maxHitPoints;
	private Integer hitPoints;
	private Integer range;
	private Integer attack;
	private Integer defense;
	private Integer damage;
	private String message;
	
	public Player(String userId, Integer playerId, String startingLocation, Integer direction, String name, Integer experience, Integer maxHitPoints, Integer hitPoints, Integer range, Integer attack, Integer defense, Integer damage) {
		this.userId = userId;
		this.playerId = playerId;
		this.startingLocation = startingLocation;
		this.location = startingLocation;
		this.direction = direction;
		this.actionTime = 0;
		this.name = name;
		this.experience = experience;
		this.maxHitPoints = maxHitPoints;
		this.hitPoints = hitPoints;
		this.range = range;
		this.attack = attack;
		this.defense = defense;
		this.damage = damage;
	}	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}		
	
	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	public String getLocation() {
		return location;
	}	

	public String getStartingLocation() {
		return startingLocation;
	}

	public void setStartingLocation(String startingLocation) {
		this.startingLocation = startingLocation;
	}

	public void setLocation(String location) {
		this.location = location;
	}		

	public Integer getDirection() {
		return direction;
	}

	public void setDirection(Integer direction) {
		this.direction = direction;
	}

	public Integer getSelectedCreatureId() {
		return selectedCreatureId;
	}

	public void setSelectedCreatureId(Integer selectedCreatureId) {
		this.selectedCreatureId = selectedCreatureId;
	}

	public Integer getSelectedSpawnPointId() {
		return selectedSpawnPointId;
	}

	public void setSelectedSpawnPointId(Integer selectedSpawnPointId) {
		this.selectedSpawnPointId = selectedSpawnPointId;
	}

	public Integer getActionTime() {
		return actionTime;
	}

	public void setActionTime(Integer actionTime) {
		this.actionTime = actionTime;
	}
	
	public Boolean canPerformAction() {
		Boolean canPerformAction = false;
		if (this.actionTime == 0) {
			canPerformAction = true;
		}
		return canPerformAction;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}	

	public Integer getMaxHitPoints() {
		return maxHitPoints;
	}

	public void setMaxHitPoints(Integer maxHitPoints) {
		this.maxHitPoints = maxHitPoints;
	}

	public Integer getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(Integer hitPoints) {
		this.hitPoints = hitPoints;
	}	

	public Integer getRange() {
		return range;
	}

	public void setRange(Integer range) {
		this.range = range;
	}	

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}	

	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}	
}
