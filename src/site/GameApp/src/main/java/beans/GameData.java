package beans;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameData {
	// Constants
	private Integer mapRows;
	private Integer mapCols;
	private Integer numberOfPlayers;
	private Integer numberSpawnPoints;
	private String mapTypes[];
	private String mapCollisionTypes[];
	private String mapMonsterCollisionTypes[];
	private String mapBlocksLineOfSightTypes[];
	private Integer timeToComplete;
	private Integer remainingTime;
	private Boolean completedGame;
	private Boolean completedLevel;
	private Boolean failedLevel;
	private HashMap<String,String> properties = new HashMap<String,String>();
	
	// Variables
	private String name;
	private Integer level;
	private String createdBy;
	private Integer map[][];
	private HashMap<String, Player> players;
	private HashMap<Integer,Creature> creatures;
	private HashMap<Integer,SpawnPoint> spawnPoints;
	private Boolean playerIds[];
	
	public GameData(String name, Integer level, String createdBy) {
		this.name = name;
		this.level = level;
		this.createdBy = createdBy;
		this.completedGame = false;
		initialize();
	}	
	
	public void initialize() {
		try {
			completedLevel = false;
			failedLevel = false;
			InputStream LevelInputStream = getClass().getClassLoader().getResourceAsStream("level"+level+".ini");	
			if (LevelInputStream != null) {
			    BufferedReader LevelReader = new BufferedReader(new InputStreamReader(LevelInputStream));
			    String line = null;
			    List<String[]> rowData = new ArrayList<String[]>();
		        while ((line = LevelReader.readLine()) != null) {
		        	line = line.trim();
		        	if (line.equals("") || line.startsWith("//")) {
		        		continue;
		        	}
		        	if (!line.contains("=") && 
		        		line.contains(",")) {
	    	        	rowData.add(line.split(","));
	    	        	continue;
		        	}
		        	if (line.contains("=")) {
		    			String property[] = line.split("=");
		    			properties.put(property[0], property[1]);	
		        	}
		        }  
	    		// Setup Map
		        String row0[] = rowData.get(0);
		        mapCols = row0.length;
		        mapRows = rowData.size();
		        map = new Integer[mapCols][mapRows];
		        for (Integer rowIdx = 0;rowIdx < mapRows;rowIdx++) {
		        	for (Integer colIdx = 0;colIdx < mapCols;colIdx++) {
		        		map[colIdx][rowIdx] = Integer.valueOf(rowData.get(rowIdx)[colIdx]);
		        	}
		        }
				mapTypes = properties.get("mapTypes").split(",");	 
				mapCollisionTypes = properties.get("mapCollisionTypes").split(",");
				mapMonsterCollisionTypes = properties.get("mapMonsterCollisionTypes").split(",");
				mapBlocksLineOfSightTypes = properties.get("mapBlocksLineOfSightTypes").split(","); 
				timeToComplete = Integer.valueOf(properties.get("timeToComplete"));
				remainingTime = timeToComplete;
		        // Setup Players
				players = new HashMap<String, Player>();
				numberOfPlayers = Integer.valueOf(properties.get("numberOfPlayers"));
				playerIds = new Boolean[numberOfPlayers];
				for (Integer idx = 0;idx < numberOfPlayers;idx++) {
					playerIds[idx] = false;
				}
				// Setup Spawn Points
				numberSpawnPoints = Integer.valueOf(properties.get("numberSpawnPoints"));
				spawnPoints = new HashMap<Integer, SpawnPoint>();
				// Setup Creatures
				creatures = new HashMap<Integer,Creature>();
			} else {
				completedGame = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public Integer getMapRows() {
		return mapRows;
	}

	public void setMapRows(Integer mapRows) {
		this.mapRows = mapRows;
	}

	public Integer getMapCols() {
		return mapCols;
	}

	public void setMapCols(Integer mapCols) {
		this.mapCols = mapCols;
	}	

	public String[] getMapTypes() {
		return mapTypes;
	}

	public void setMapTypes(String[] mapTypes) {
		this.mapTypes = mapTypes;
	}

	public String[] getMapCollisionTypes() {
		return mapCollisionTypes;
	}

	public void setMapCollisionTypes(String[] mapCollisionTypes) {
		this.mapCollisionTypes = mapCollisionTypes;
	}	

	public String[] getMapMonsterCollisionTypes() {
		return mapMonsterCollisionTypes;
	}

	public void setMapMonsterCollisionTypes(String[] mapMonsterCollisionTypes) {
		this.mapMonsterCollisionTypes = mapMonsterCollisionTypes;
	}	

	public String[] getMapBlocksLineOfSightTypes() {
		return mapBlocksLineOfSightTypes;
	}

	public void setMapBlocksLineOfSightTypes(String[] mapBlocksLineOfSightTypes) {
		this.mapBlocksLineOfSightTypes = mapBlocksLineOfSightTypes;
	}			

	public Integer getTimeToComplete() {
		return timeToComplete;
	}

	public void setTimeToComplete(Integer timeToComplete) {
		this.timeToComplete = timeToComplete;
	}	

	public Integer getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(Integer remainingTime) {
		this.remainingTime = remainingTime;
	}

	public Integer getNumberOfPlayers() {
		return numberOfPlayers;
	}

	public void setNumberOfPlayers(Integer numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	public HashMap<String, String> getProperties() {
		return properties;
	}

	public void setProperties(HashMap<String, String> properties) {
		this.properties = properties;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}		

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}		
	
	public Boolean getCompletedGame() {
		return completedGame;
	}

	public void setCompletedGame(Boolean completedGame) {
		this.completedGame = completedGame;
	}	

	public Boolean getCompletedLevel() {
		return completedLevel;
	}

	public void setCompletedLevel(Boolean completedLevel) {
		this.completedLevel = completedLevel;
	}	

	public Boolean getFailedLevel() {
		return failedLevel;
	}

	public void setFailedLevel(Boolean failedLevel) {
		this.failedLevel = failedLevel;
	}

	public Integer[][] getMap() {
		return map;
	}

	public void setMap(Integer[][] map) {
		this.map = map;
	}		
	
	public String[] getPlayerColors() {
		return this.properties.get("playerColors").split(",");
	}
	
	public HashMap<String, Player> getPlayers() {
		return players;
	}

	public void setPlayers(HashMap<String, Player> players) {
		this.players = players;
	}
	
	public Player[] getPlayerList() {
		Player results[] = new Player[numberOfPlayers];
		for (Player player:getPlayers().values()) {
			results[player.getPlayerId()] = player;
		}
		return results;
	}
	
	public Integer getPlayerCount() {
		Integer playerCount = 0;
		for (Player player:getPlayerList()) {
			if (player != null) {
				playerCount += 1;
			}
		}	
		return playerCount;
	}

	public Boolean[] getPlayerIds() {
		return playerIds;
	}

	public void setPlayerIds(Boolean[] playerIds) {
		this.playerIds = playerIds;
	}
	
	public Integer getAvailPlayerId() {
		Integer results = null;
		for (Integer idx = 0;idx < numberOfPlayers; idx++) {
			if (playerIds[idx] == false) {
				playerIds[idx] = true;
				results = idx;
				break;
			}
		}
		return results;
	}
	
	public void addPlayer(Player player) {
		getPlayers().put(player.getUserId(), player);
	}

	public Integer getNumberSpawnPoints() {
		return numberSpawnPoints;
	}

	public void setNumberSpawnPoints(Integer numberSpawnPoints) {
		this.numberSpawnPoints = numberSpawnPoints;
	}	

	public HashMap<Integer,SpawnPoint> getSpawnPoints() {
		return spawnPoints;
	}

	public void setSpawnPoints(HashMap<Integer,SpawnPoint> spawnPoints) {
		this.spawnPoints = spawnPoints;
	}
	
	public List<SpawnPoint> getSpawnPointList() {
		List<SpawnPoint> spawnPointList = new ArrayList<SpawnPoint>();
		for (SpawnPoint spawnPoint:spawnPoints.values()) {
			spawnPointList.add(spawnPoint);
		}
		return spawnPointList;
	}	

	public HashMap<Integer,Creature> getCreatures() {
		return creatures;
	}

	public void setCreatures(HashMap<Integer,Creature> creatures) {
		this.creatures = creatures;
	}
	
	public List<Creature> getCreatureList() {
		List<Creature> creatureList = new ArrayList<Creature>();
		for (Creature creature:creatures.values()) {
			creatureList.add(creature);
		}
		return creatureList;
	}
}
