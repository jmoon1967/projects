package beans;

import java.util.ArrayList;
import java.util.List;

public class SpawnPoint {

	private Integer id;
	private String color;
	private String location;
	private Integer hitPoints;
	private Integer maxHitPoints;
	private Integer spawnCycle;
	private Integer startingCreatures;
	private Integer maxCreatures;
	private Integer currentCycle;
	private String message;
	private List<Integer> creatureIdList;
	
	public SpawnPoint(Integer id, String color, String location, Integer maxHitPoints, Integer spawnCycle, Integer startingCreatures, Integer maxCreatures) {
		this.id = id;
		this.color = color;
		this.location = location;
		this.hitPoints = maxHitPoints;
		this.maxHitPoints = maxHitPoints;
		this.spawnCycle = spawnCycle;
		this.startingCreatures = startingCreatures;
		this.maxCreatures = maxCreatures;
		this.currentCycle = 0;
		this.creatureIdList = new ArrayList<Integer>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}		

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}	
	
	public Integer getSpawnCycle() {
		return spawnCycle;
	}

	public void setSpawnCycle(Integer spawnCycle) {
		this.spawnCycle = spawnCycle;
	}	

	public Integer getMaxHitPoints() {
		return maxHitPoints;
	}

	public void setMaxHitPoints(Integer maxHitPoints) {
		this.maxHitPoints = maxHitPoints;
	}

	public Integer getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(Integer hitPoints) {
		this.hitPoints = hitPoints;
	}	

	public Integer getMaxCreatures() {
		return maxCreatures;
	}

	public void setMaxCreatures(Integer maxCreatures) {
		this.maxCreatures = maxCreatures;
	}	

	public Integer getStartingCreatures() {
		return startingCreatures;
	}

	public void setStartingCreatures(Integer startingCreatures) {
		this.startingCreatures = startingCreatures;
	}

	public Integer getCurrentCycle() {
		return currentCycle;
	}

	public void setCurrentCycle(Integer currentCycle) {
		this.currentCycle = currentCycle;
	}	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Integer> getCreatureIdList() {
		return creatureIdList;
	}

	public void setCreatureIdList(List<Integer> creatureIdList) {
		this.creatureIdList = creatureIdList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpawnPoint other = (SpawnPoint) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
