package beans;

import java.util.Random;

import actions.GameAction;
import utilities.CommonUtility;

public class Game extends Thread {

	private GameData data;
	private GameAction action;
	private Boolean running = false;
	private Integer lastCreatureId = 0;
	private Integer gameCycle = 1000;
	
	public Game(String name, Integer level, String createdBy, GameAction action) {
		this.data = new GameData(name, level, createdBy);
		this.action = action;
	}

	public GameData getData() {
		return data;
	}

	public void setData(GameData data) {
		this.data = data;
	}	
	
	public GameAction getAction() {
		return action;
	}

	public void setAction(GameAction action) {
		this.action = action;
	}
	
	public void nextLevel() {
		getData().setLevel(getData().getLevel()+1);
		Player[] players = getData().getPlayerList();
		getData().initialize();
		for (Player player:players) {
			if (player != null) {
				player.setStartingLocation(getData().getProperties().get("locationPlayer"+player.getPlayerId()));
				player.setLocation(player.getStartingLocation());
				getData().addPlayer(player);
			}
		}
		getData().setCompletedGame(false);		
		init();
	}

	public void run() {
		running = true;
		if (!getData().getCompletedGame()) {
			init();
			while (running) {
				try {
					sleep(gameCycle);
					update();
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
		}
	}
	
	public void delete() {
		running = false;
	}
	
	private void init() {
		for (Integer idx = 0;idx < data.getNumberSpawnPoints();idx++) {
			SpawnPoint spawnPoint = new SpawnPoint(idx,data.getProperties().get("colorSpawnPoint"+idx),data.getProperties().get("locationSpawnPoint"+idx),Integer.valueOf(data.getProperties().get("hitPointsSpawnPoint"+idx)),Integer.valueOf(data.getProperties().get("cycleSpawnPoint"+idx)),Integer.valueOf(data.getProperties().get("startingCreaturesSpawnPoint"+idx)),Integer.valueOf(data.getProperties().get("maxCreaturesSpawnPoint"+idx)));
			data.getSpawnPoints().put(spawnPoint.getId(),spawnPoint);
			if (spawnPoint.getStartingCreatures() > 0) {
				for (Integer SCidx = 0;SCidx < spawnPoint.getStartingCreatures();SCidx++) {
					Creature creature = 
							new Creature(
									lastCreatureId+=1,
									data.getProperties().get("colorCreature"+spawnPoint.getId()),
									spawnPoint.getLocation(),
									new Integer((int)(new Random().nextInt(8)+1)),
									Integer.valueOf(data.getProperties().get("hitPointsCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("hitPointsCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("rangeCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("attackCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("defenseCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("damageCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("alertRangeCreature"+spawnPoint.getId())),
									Integer.valueOf(data.getProperties().get("rewardXpCreature"+spawnPoint.getId())),
									spawnPoint);
					data.getCreatures().put(creature.getId(),creature);
					spawnPoint.getCreatureIdList().add(creature.getId());					
				}
			}
		}
	}
	
	private void update() {
		if (data.getPlayerCount() > 0) {
			// Check for Completing Level
			if (getData().getCreatureList().size() <= 0 &&
				getData().getSpawnPoints().size() <= 0) {
				getData().setCompletedLevel(true);
			}
			// Check for FailedLevel
			if (getData().getRemainingTime() <= 0) {
				getData().setFailedLevel(true);
			}
			if (!(getData().getCompletedLevel()) && 
				!(getData().getFailedLevel()) &&
				!(getData().getCompletedGame())) {
				getData().setRemainingTime(getData().getRemainingTime()-1);
				// Reset Player Messages
				for (Player player: data.getPlayerList()) {
					if (player != null) {
						player.setMessage(null);
					}
				}
				// Spawn Creatures
				for (SpawnPoint spawnPoint:data.getSpawnPoints().values()) {
					if (spawnPoint != null) {
						spawnPoint.setMessage(null);
						spawnPoint.setCurrentCycle(spawnPoint.getCurrentCycle()+1);
						if (spawnPoint.getCurrentCycle() == spawnPoint.getSpawnCycle()) {
							spawnPoint.setCurrentCycle(0);
							if (spawnPoint.getMaxCreatures() == 0 ||
								spawnPoint.getCreatureIdList().size() < spawnPoint.getMaxCreatures()*getData().getPlayerCount()) {
								Creature creature = 
										new Creature(
												lastCreatureId+=1,
												data.getProperties().get("colorCreature"+spawnPoint.getId()),
												spawnPoint.getLocation(),
												new Integer((int)(new Random().nextInt(8)+1)),
												Integer.valueOf(data.getProperties().get("hitPointsCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("hitPointsCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("rangeCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("attackCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("defenseCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("damageCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("alertRangeCreature"+spawnPoint.getId())),
												Integer.valueOf(data.getProperties().get("rewardXpCreature"+spawnPoint.getId())),
												spawnPoint);
								data.getCreatures().put(creature.getId(),creature);
								spawnPoint.getCreatureIdList().add(creature.getId());
							}
						}
					}
				}
				// Attack Players and Move Creatures
				for (Creature creature:data.getCreatureList()) {
					if (creature != null) {
						creature.setMessage(null);
						// Move Creature
						String locationArray[] = creature.getLocation().split(",");
						Integer direction = getDirection(creature);
						creature.setDirection(direction);
						switch(direction) {
							case 1: locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;
							case 2: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;
							case 3: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);break;
							case 4: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])+1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
							case 5: locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
							case 6: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])+1);break;
							case 7: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);break;
							case 8: locationArray[0]=String.valueOf(Integer.valueOf(locationArray[0])-1);locationArray[1]=String.valueOf(Integer.valueOf(locationArray[1])-1);break;				
						}
						if (!CommonUtility.checkCollision(data.getName(), locationArray, true)) {
							creature.setLocation(locationArray[0]+","+locationArray[1]);	
						}	
						// Attack Player
						if (creature.getActionTime() > 0) {
							creature.setActionTime(creature.getActionTime()-1);
						}
						if (creature.canPerformAction()) {
							String creatureLocation[] = creature.getLocation().split(",");
							for (Player player:data.getPlayerList()) {
								if (player != null) {
									String playerLocation[] = player.getLocation().split(",");
									if (Integer.valueOf(playerLocation[0]) >= Integer.valueOf(creatureLocation[0])-creature.getRange() &&
										Integer.valueOf(playerLocation[0]) <= Integer.valueOf(creatureLocation[0])+creature.getRange() &&
										Integer.valueOf(playerLocation[1]) >= Integer.valueOf(creatureLocation[1])-creature.getRange() &&
										Integer.valueOf(playerLocation[1]) <= Integer.valueOf(creatureLocation[1])+creature.getRange()) {
										attackPlayer(player, creature);
										break;
									}
								}
							}
						}
					}
				}	
				for (Player player:data.getPlayerList()) {
					if (player != null) {
						if (player.getActionTime() > 0) {
							player.setActionTime(player.getActionTime()-1);
						}
						// Heal Players
						if (player.getStartingLocation().equals(player.getLocation())) {
							if (player.getHitPoints() < player.getMaxHitPoints()) {
								player.setHitPoints(player.getHitPoints()+1);
								player.setMessage("Healing");
							}
						}
						// Players Attack
						if (player.canPerformAction()) {
							if (player.getSelectedCreatureId() != null) {
								Creature creature = getData().getCreatures().get(player.getSelectedCreatureId());
								if (creature != null) {
									attackCreature(player, creature);
								}
							}
							if (player.getSelectedSpawnPointId() != null) {
								SpawnPoint spawnPoint = getData().getSpawnPoints().get(player.getSelectedSpawnPointId());
								if (spawnPoint != null) {
									attackSpawnPoint(player, spawnPoint);
								}
							}
						}
					}
				}	
			}
			try {
				action.publishGame(data.getName());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private Integer getDirection(Creature creature) {
		Integer results =  new Integer((int)(new Random().nextInt(8)+1));
		String creatureLocation[] = creature.getLocation().split(",");
		Player closestPlayer = null;
		for (Player player:data.getPlayerList()) {
			if (player != null) {
				String playerLocation[] = player.getLocation().split(",");
				if (hasLineOfSight(creatureLocation, playerLocation, creature.getAlertRange())) {
					if (closestPlayer == null) {
						closestPlayer = player;
					} else {
						String closestLocation[] = closestPlayer.getLocation().split(",");
						if (Integer.valueOf(creatureLocation[0])-Integer.valueOf(playerLocation[0]) <
						    Integer.valueOf(creatureLocation[0])-Integer.valueOf(closestLocation[0]) &&
						    Integer.valueOf(creatureLocation[1])-Integer.valueOf(playerLocation[1]) <
						    Integer.valueOf(creatureLocation[1])-Integer.valueOf(closestLocation[0])){
						    closestPlayer = player;
						}
					}
				}
			}			
		}
		if (closestPlayer != null) {
			String playerLocation[] = closestPlayer.getLocation().split(",");
			Boolean notChanged = true;
			// North
			if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) < Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0]) &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])-1) {
					results = 1;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;						
				}												
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;						
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;						
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;	
				}
			}
			// Northeast
			if (Integer.valueOf(playerLocation[0]) > Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) < Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])+1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])-1) {
					results = 2;
					notChanged = false;
				}						
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;						
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;					
				}						
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;	
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}
			}	
			// East
			if (Integer.valueOf(playerLocation[0]) > Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])+1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])) {
					results = 3;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;	
				}							
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}
			}	
			// Southeast
			if (Integer.valueOf(playerLocation[0]) > Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) > Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])+1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])+1) {
					results = 4;
					notChanged = false;
				}						
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;	
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}	
			}	
			// South
			if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) > Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0]) &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])+1) {
					results = 5;
					notChanged = false;
				}						
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}
			}	
			// Southwest
			if (Integer.valueOf(playerLocation[0]) < Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) > Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])-1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])+1) {
					results = 6;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
			}	
			// West
			if (Integer.valueOf(playerLocation[0]) < Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])-1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])) {
					results = 7;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;
					notChanged = false;
				}	
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;
				}
			}		
			// Northwest
			if (Integer.valueOf(playerLocation[0]) < Integer.valueOf(creatureLocation[0]) &&
				Integer.valueOf(playerLocation[1]) < Integer.valueOf(creatureLocation[1])) {
				if (Integer.valueOf(playerLocation[0]) == Integer.valueOf(creatureLocation[0])-1 &&
					Integer.valueOf(playerLocation[1]) == Integer.valueOf(creatureLocation[1])-1) {
					results = 8;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 8;						
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 1;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 7;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])-1) }, true)) {
					results = 2;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])-1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 6;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])) }, true)) {
					results = 3;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 5;
					notChanged = false;
				}
				if (notChanged &&
					!CommonUtility.checkCollision(data.getName(), new String[] { String.valueOf(Integer.valueOf(creatureLocation[0])+1), String.valueOf(Integer.valueOf(creatureLocation[1])+1) }, true)) {
					results = 4;
					notChanged = false;
				}
			}
		}
		return results;
	}
	
	public void attackCreature(Player player, Creature creature) {
		String playerLocation[] = player.getLocation().split(",");
		String creatureLocation[] = creature.getLocation().split(",");
		if (hasLineOfSight(playerLocation, creatureLocation, player.getRange())) {
			if (Integer.valueOf(creatureLocation[0]) >= Integer.valueOf(playerLocation[0])-player.getRange() &&
				Integer.valueOf(creatureLocation[0]) <= Integer.valueOf(playerLocation[0])+player.getRange() &&
				Integer.valueOf(creatureLocation[1]) >= Integer.valueOf(playerLocation[1])-player.getRange() &&
				Integer.valueOf(creatureLocation[1]) <= Integer.valueOf(playerLocation[1])+player.getRange()) {
				player.setActionTime(2);
				Integer attackRoll = new Integer((int)(new Random().nextInt(10)+1));
				Integer defenseRoll = new Integer((int)(new Random().nextInt(10)+1));
				if (attackRoll > 1 &&
					defenseRoll < 10 &&
					attackRoll+player.getAttack() > defenseRoll+creature.getDefense()) {
					player.setMessage("Attack: Hit");
					creature.setHitPoints(creature.getHitPoints()-player.getDamage());
					if (creature.getHitPoints() <= 0) {
						player.setExperience(player.getExperience()+creature.getRewardXp());
						getData().getCreatures().remove(creature.getId());
						creature.getSpawnPoint().getCreatureIdList().remove(creature.getId());
					}
				} else {
					player.setMessage("Attack: Missed");
				}		
			}
		}
	}
	
	public void attackSpawnPoint(Player player, SpawnPoint spawnPoint) {
		String playerLocation[] = player.getLocation().split(",");
		String spawnPointLocation[] = spawnPoint.getLocation().split(",");
		if (hasLineOfSight(playerLocation, spawnPointLocation, player.getRange())) {
			if (Integer.valueOf(spawnPointLocation[0]) >= Integer.valueOf(playerLocation[0])-player.getRange() &&
				Integer.valueOf(spawnPointLocation[0]) <= Integer.valueOf(playerLocation[0])+player.getRange() &&
				Integer.valueOf(spawnPointLocation[1]) >= Integer.valueOf(playerLocation[1])-player.getRange() &&
				Integer.valueOf(spawnPointLocation[1]) <= Integer.valueOf(playerLocation[1])+player.getRange()) {
				Integer attackRoll = new Integer((int)(new Random().nextInt(10)+1));
				Integer defenseRoll = new Integer((int)(new Random().nextInt(10)+1));
				player.setActionTime(2);
				if (attackRoll > 1 &&
					defenseRoll < 10 &&
					attackRoll+player.getAttack() > defenseRoll) {
					player.setMessage("Attack: Hit");
					spawnPoint.setHitPoints(spawnPoint.getHitPoints()-player.getDamage());
					if (spawnPoint.getHitPoints() <= 0) {
						getData().getSpawnPoints().remove(spawnPoint.getId());
					}
				} else {
					player.setMessage("Attack: Missed");
				}		
			}
		}
	}	

	private void attackPlayer(Player player, Creature creature) {
		String playerLocation[] = player.getLocation().split(",");
		String creatureLocation[] = creature.getLocation().split(",");		
		if (hasLineOfSight(creatureLocation, playerLocation, creature.getRange())) {
			Integer attackRoll = new Integer((int)(new Random().nextInt(10)+1));
			Integer defenseRoll = new Integer((int)(new Random().nextInt(10)+1));
			creature.setActionTime(2);
			if (attackRoll > 1 &&
				defenseRoll < 10 &&
				attackRoll+creature.getAttack() > defenseRoll+player.getDefense()) {
				creature.setMessage("Attack: Hit");
				player.setHitPoints(player.getHitPoints()-creature.getDamage());
				if (player.getHitPoints() <= 0) {
					player.setLocation(player.getStartingLocation());
					if (player.getExperience() > 0) {
						player.setExperience(new Integer((int)Math.floor(player.getExperience()/2)));
					}
				}
			} else {
				creature.setMessage("Attack: Missed");
			}
		}
	}	
	
	private Boolean hasLineOfSight(String attackerLocation[], String defenderLocation[], Integer range) {
		Boolean results = true;
		if (range > 1 &&
			((Math.abs(Integer.valueOf(attackerLocation[0])-Integer.valueOf(defenderLocation[0]))) > 1 ||
			 (Math.abs(Integer.valueOf(attackerLocation[1])-Integer.valueOf(defenderLocation[1]))) > 1)) {
			// North
			if (Integer.valueOf(defenderLocation[0]) == Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) < Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])][Integer.valueOf(attackerLocation[1])-1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { attackerLocation[0],Integer.toString((Integer.valueOf(attackerLocation[1])-1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}
			// Northeast
			if (Integer.valueOf(defenderLocation[0]) > Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) < Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])+1][Integer.valueOf(attackerLocation[1])-1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])+1),Integer.toString((Integer.valueOf(attackerLocation[1])-1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// East
			if (Integer.valueOf(defenderLocation[0]) > Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) == Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])+1][Integer.valueOf(attackerLocation[1])]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])+1),Integer.toString((Integer.valueOf(attackerLocation[1]))) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// Southeast
			if (Integer.valueOf(defenderLocation[0]) > Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) > Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])+1][Integer.valueOf(attackerLocation[1])+1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])+1),Integer.toString((Integer.valueOf(attackerLocation[1])+1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// South
			if (Integer.valueOf(defenderLocation[0]) == Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) > Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])][Integer.valueOf(attackerLocation[1])+1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])),Integer.toString((Integer.valueOf(attackerLocation[1])+1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// Southwest
			if (Integer.valueOf(defenderLocation[0]) < Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) > Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])-1][Integer.valueOf(attackerLocation[1])+1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])-1),Integer.toString((Integer.valueOf(attackerLocation[1])+1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// West
			if (Integer.valueOf(defenderLocation[0]) < Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) == Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])-1][Integer.valueOf(attackerLocation[1])]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])-1),Integer.toString((Integer.valueOf(attackerLocation[1]))) }, defenderLocation, Integer.valueOf(range-1));
				}
			}	
			// Northwest
			if (Integer.valueOf(defenderLocation[0]) < Integer.valueOf(attackerLocation[0]) &&
				Integer.valueOf(defenderLocation[1]) < Integer.valueOf(attackerLocation[1])){
				for (String collisionType:getData().getMapBlocksLineOfSightTypes()) {
					if ((getData().getMap()[Integer.valueOf(attackerLocation[0])-1][Integer.valueOf(attackerLocation[1])-1]) == Integer.valueOf(collisionType)) {		
						results = false;
						break;
					} 		
				}	
				if (results) {
					results = hasLineOfSight(new String[] { Integer.toString(Integer.valueOf(attackerLocation[0])-1),Integer.toString((Integer.valueOf(attackerLocation[1])-1)) }, defenderLocation, Integer.valueOf(range-1));
				}
			}				
		}
		return results;
	}	
}
