package beans;

public class Creature {

	private Integer id;
	private String color;
	private String location;
	private Integer direction;
	private Integer actionTime;
	private Integer hitPoints;
	private Integer maxHitPoints;
	private Integer range;
	private Integer attack;
	private Integer defense;
	private Integer damage;
	private Integer alertRange;
	private Integer rewardXp;
	private String message;
	private SpawnPoint spawnPoint;
	
	public Creature(Integer id, String color, String location, Integer direction, Integer maxHitPoints, Integer hitPoints, Integer range, Integer attack, Integer defense, Integer damage, Integer alertRange, Integer rewardXp, SpawnPoint spawnPoint) {
		this.id = id;
		this.color = color;
		this.location = location;
		this.direction = direction;
		this.actionTime= 0;
		this.maxHitPoints = maxHitPoints;
		this.hitPoints = hitPoints;
		this.range = range;
		this.attack = attack;
		this.defense = defense;
		this.damage = damage;
		this.alertRange = alertRange;
		this.rewardXp = rewardXp;
		this.spawnPoint = spawnPoint;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}		

	public Integer getDirection() {
		return direction;
	}

	public void setDirection(Integer direction) {
		this.direction = direction;
	}

	public Integer getActionTime() {
		return actionTime;
	}

	public void setActionTime(Integer actionTime) {
		this.actionTime = actionTime;
	}
	
	public Boolean canPerformAction() {
		Boolean canPerformAction = false;
		if (actionTime == 0) {
			canPerformAction = true;
		}
		return canPerformAction;
	}

	public Integer getHitPoints() {
		return hitPoints;
	}

	public void setHitPoints(Integer hitPoints) {
		this.hitPoints = hitPoints;
	}	

	public Integer getMaxHitPoints() {
		return maxHitPoints;
	}

	public void setMaxHitPoints(Integer maxHitPoints) {
		this.maxHitPoints = maxHitPoints;
	}	

	public Integer getRange() {
		return range;
	}

	public void setRange(Integer range) {
		this.range = range;
	}

	public Integer getAttack() {
		return attack;
	}

	public void setAttack(Integer attack) {
		this.attack = attack;
	}

	public Integer getDefense() {
		return defense;
	}

	public void setDefense(Integer defense) {
		this.defense = defense;
	}	

	public Integer getDamage() {
		return damage;
	}

	public void setDamage(Integer damage) {
		this.damage = damage;
	}

	public Integer getAlertRange() {
		return alertRange;
	}

	public void setAlertRange(Integer alertRange) {
		this.alertRange = alertRange;
	}

	public Integer getRewardXp() {
		return rewardXp;
	}

	public void setRewardXp(Integer rewardXp) {
		this.rewardXp = rewardXp;
	}	

	public SpawnPoint getSpawnPoint() {
		return spawnPoint;
	}

	public void setSpawnPoint(SpawnPoint spawnPoint) {
		this.spawnPoint = spawnPoint;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Creature other = (Creature) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}	
}
