package exceptions;

public class InvalidRequestException extends Exception {

	private static final long serialVersionUID = -5084399091238203467L;

	public InvalidRequestException(String message) {
		super(message);
	}
}
